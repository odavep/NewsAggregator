﻿using System;

namespace Classes {
    /// <summary>
    /// Class to represent position for each piece of data in a class
    /// To be used in conjunction with article for each source
    /// </summary>
    public class Config {
        public int ConfigID { get; set; }

        public string Source { get; set; }
        public int Title { get; set; }
        public int Description { get; set; }
        public int ImageLink { get; set; }
        public int ArticleLink { get; set; }
        public int PublishDate { get; set; }
    }
}
