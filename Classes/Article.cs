﻿using System;

namespace Classes {
    /// <summary>
    /// Class to represent Articles in C#
    /// </summary>
    public class Article {
        public int ArticleID { get; set; }

        public string Title { get; set; }
        public string Description { get; set; }
        public string ImageLink { get; set; }
        public string ArticleLink { get; set; }
        public string Source { get; set; }
        public DateTime PublishDate { get; set; }
    }
}
