# NewsAggregator

A news article aggregator using RSS feeds to extract articles from differing sources. These aggregated articles are displayed on a website for the user to see.

## In-depth

Articles will extracted from an RSS Feed (which is in a XML format) via a C# application, which will put each articke into standard CLR objects. These CLR objects will then be committed to a database for persistence even when the server is off. While the database is periodically updated, a web server will display articles from the database to the user in a nice format. 

This therefore provides a clear distinction between the frontend and the backend: 

* Aggregator of articles (purely backend)
* Database (purely backend)
* Web server (backend funnel to frontend)
* Web page (purely frontend)

__NOTE__: Articles that have the same title in differing sources, the first to be inserted will be used. This is a ~~bug for the technical debt pile~~ feature now.

## Languages and services used

* C# for the aggregator
* C#/Node.js for the web server
* MySQL for the database

## Technical debt - components that I need to spend longer thinking about/refactoring

* Articles having the same title will have a collision due to database design (Title is a unique field), thus only the first inserted article will stay, the other deleted.
* Program is a bit slow when inserting due to constant try{}check{}'ing. Will become a proper big problem in certain scenarios where you might have a large amount of articles before each update
