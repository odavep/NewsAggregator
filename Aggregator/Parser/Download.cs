﻿using System.Net;
using System.Threading.Tasks;

namespace Aggregator {
    public static class Download {
        /// <summary>
        /// Download source from a given site synchronously
        /// </summary>
        /// <param name="site">Site to download from</param>
        /// <returns></returns>
        public static string DownloadXMLSync(string site) {
            using (var client = new WebClient()) {
                return client.DownloadString(site);
            }
        }

        /// <summary>
        /// Download source from a given site asynchronously
        /// </summary>
        /// <param name="site">Site to download from</param>
        /// <returns></returns>
        public static Task<string> DownloadXML(string site) {
            using (var client = new WebClient()) {
                /* TODO: Find some way to circumvent creating a new WebClient every download
                 *2019-02-03 23:43*/
                return client.DownloadStringTaskAsync(site);
            }
        }
    }
}
