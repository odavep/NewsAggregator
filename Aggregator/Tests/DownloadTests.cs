using System;
using System.Xml;
using System.Xml.Linq;
using System.Collections.Generic;
using Aggregator;
using Xunit;

namespace Aggregator.Tests {
    public class DownloadTests {
        [Fact]
        public void Test_DownloadXMLSync() {
            string xml = Download.DownloadXMLSync("http://feeds.bbci.co.uk/news/rss.xml");
            Assert.True(xml != null);
            Assert.True(xml != " ");
            Assert.True(XDocument.Parse(xml) != null);
        }

        [Fact]
        public async void Test_DownloadXML() {
            string xml = await Download.DownloadXML("http://feeds.bbci.co.uk/news/rss.xml");
            Assert.True(xml != null);
            Assert.True(xml != " ");
            Assert.True(XDocument.Parse(xml) != null);
        }
    }
}
