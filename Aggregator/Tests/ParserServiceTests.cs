using System;
using System.Linq;
using System.Threading.Tasks;
using System.Collections.Generic;
using Aggregator;
using Classes;
using Xunit;

namespace Aggregator.Tests {
    public class ParserServiceTests {
        private static Config TestConfig = new Config()
        {
            Source = "BBC",
            Title = 0,
            Description = 1,
            ArticleLink = 2,
            ImageLink = 5,
            PublishDate = 4
        };

        [Fact]
        public async void Test_Parse() {
            string xml = await Download.DownloadXML("http://feeds.bbci.co.uk/news/rss.xml");
            List<Article> articles = ParserService.Parse(xml, TestConfig).ToList();
            Assert.True(articles != null);
            Assert.True(articles.Count > 0);
        }
    }
}