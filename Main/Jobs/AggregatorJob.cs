using System.Linq;
using System.Threading.Tasks;
using System.Collections.Generic;

using Microsoft.Extensions.DependencyInjection;
using Microsoft.EntityFrameworkCore;

using Classes;
using Database;
using Aggregator;

namespace Main.Jobs {
    public interface IAggregatorJob {
        Task Start();
    }

    public class AggregatorJob : IAggregatorJob {
        private DatabaseContext Context;
        public AggregatorJob(DatabaseContext context) {
            Context = context;
        }

        private static List<Config> configs = new List<Config> {
            new Config {
                Source = "BBC",
                Title = 0,
                Description = 1,
                ArticleLink = 2,
                ImageLink = 5,
                PublishDate = 4
            },
            new Config {
                Source = "Sky",
                Title = 0,
                Description = 2,
                ArticleLink = 1,
                ImageLink = 7,
                PublishDate = 3
            },
            new Config {
                Source = "ABC",
                Title = 7,
                Description = 11,
                ArticleLink = 8,
                ImageLink = 0,
                PublishDate = 10
            }
        };

        private static List<string> sites = new List<string> {
            "http://feeds.bbci.co.uk/news/rss.xml",
            "http://feeds.skynews.com/feeds/rss/world.xml",
            "https://abcnews.go.com/abcnews/topstories"
        };

        public async Task Start() {
            var articles = await parseMultiple();
            await Context.InsertArticles(articles);
        }

        private async Task<List<Article>> parseMultiple() {
            var xmls = new List<string>();
            foreach (string site in sites) {
                xmls.Add(await Download.DownloadXML(site));
            }

            return ParserService.ParseMultiple(xmls.ToArray(), configs).ToList();
        }
    }
}