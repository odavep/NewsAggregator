class Article {
  Container: HTMLElement;
  Title: HTMLElement;
  Image: HTMLElement;

  constructor(title: string, description: string, article_link: string, image_link: string) {
    // object initialisation
    this.Container = document.createElement('div');
    this.Title = document.createElement('h1');
    this.Image = new Image();
    let link = document.createElement('a');

    // attributes
    link.innerText = title;
    link.setAttribute('href', article_link);
    this.Image.setAttribute('src', image_link);
    this.Image.setAttribute('title', description);
    this.Container.className = "Frame";

    // make unit
    this.Title.appendChild(link);
    this.Container.appendChild(this.Title);
    this.Container.appendChild(this.Image);
  }

  visualise(root: HTMLElement): void {
    // visualise the article for document
    root.appendChild(this.Container);
  }
}

let root = document.getElementById('root');

let title = "Amazon reconsiders building new headquarters in New York City: WaPo";
let description = "Description";
let article = "https://www.wikipedia.org/";
let image = "https://s.abcnews.com/images/Business/long-island-city-gty-jc-181112_hpMain_4x3t_384.jpg";

let articles: Array<Article> = [
  new Article(title, description, article, image),
  new Article(title, description, article, image),
  new Article(title, description, article, image),
  new Article(title, description, article, image),
  new Article(title, description, article, image),
  new Article(title, description, article, image),
  new Article(title, description, article, image)
];

articles.forEach(article => article.visualise(root));
