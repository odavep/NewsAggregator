﻿using System;
using System.Linq;
using System.Threading.Tasks;
using System.Collections.Generic;

using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata;
using Classes;

namespace Database {
    /// <summary>
    /// Database context for MySQL
    /// </summary>
    public partial class DatabaseContext : DbContext {
        public DatabaseContext() { }

        public DatabaseContext(DbContextOptions<DatabaseContext> options)
            : base(options) { }

        public virtual DbSet<Article> Articles { get; set; }

        /// <summary>
        /// Insert multiple articles into database
        /// </summary>
        /// <param name="articles">articles to insert</param>
        public async Task InsertArticles(IEnumerable<Article> articles) {
            foreach (Article article in articles) {
                try {
                    await Articles.AddAsync(article);
                    await SaveChangesAsync();
                }
                catch (Microsoft.EntityFrameworkCore.DbUpdateException) {
                    continue;
                }
            }
        }

        protected override void OnConfiguring(DbContextOptionsBuilder optionsBuilder) { }

        /// <summary>
        /// Model for MySQL database to entity
        /// </summary>
        protected override void OnModelCreating(ModelBuilder modelBuilder) {
            modelBuilder.HasAnnotation("ProductVersion", "2.2.1-servicing-10028");

            modelBuilder.Entity<Article>(entity => {
                entity.HasKey(e => e.ArticleID);

                entity.ToTable("articles", "NewsAggregator");

                entity.Property(e => e.ArticleID)
                    .HasColumnName("article_id")
                    .HasColumnType("int(11)")
                    .ValueGeneratedOnAdd();

                entity.Property(e => e.ArticleLink)
                    .HasColumnName("article_link")
                    .HasMaxLength(128)
                    .IsUnicode(false);

                entity.Property(e => e.Description)
                    .HasColumnName("description")
                    .HasMaxLength(512)
                    .IsUnicode(false);

                entity.Property(e => e.ImageLink)
                    .HasColumnName("image_link")
                    .HasMaxLength(128)
                    .IsUnicode(false);

                entity.Property(e => e.PublishDate)
                    .HasColumnName("publish_date")
                    .HasColumnType("date")
                    .ValueGeneratedNever();

                entity.Property(e => e.Source)
                    .HasColumnName("source")
                    .HasMaxLength(8)
                    .IsUnicode(false);

                entity.Property(e => e.Title)
                    .HasColumnName("title")
                    .HasMaxLength(255)
                    .IsUnicode(false);
            });
        }
    }
}
