CREATE TABLE articles (
    article_id INT NOT NULL AUTO_INCREMENT,
    title VARCHAR(255) UNIQUE,
    description VARCHAR(512),
    image_link VARCHAR(128),
    article_link VARCHAR(128),
    source VARCHAR(8),
    publish_date DATE,

    PRIMARY KEY(article_id)
)