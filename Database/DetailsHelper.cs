using System.IO;
using System.Linq;
using System.Threading.Tasks;
using System.Collections.Generic;
using Newtonsoft.Json;

namespace Database {
    public static class DetailsHelper {
        public static async Task<Dictionary<string, string>> GetDetails() {
            string json = string.Join('\n', await File.ReadAllLinesAsync("details.json"));
            return JsonConvert.DeserializeObject<Dictionary<string, string>>(json);
        }
    }
}